from pytest import *
from TTT.TicTacToe import *

def test_coupsPossibles():
    # Pour un jeu empty
    jeu = TicTacToe()
    assert jeu.available() == [0, 1, 2, 3, 4, 5, 6, 7, 8]

    # Pour une grid partiellement remplie
    jeu.grid = ['x', 'o', ' ', 'x', ' ', ' ', ' ', 'o', 'x']
    assert jeu.available() == [2, 4, 5, 6]

    # Pour une grid pleine
    jeu.grid = ['x', 'o', 'o', 'x', 'o', 'x', 'x', 'o', 'x']
    assert jeu.available() == []

def test_winning():
    # Pour un jeu empty
    jeu = TicTacToe()
    assert (jeu.winning(False) == False) and (jeu.winning(True) == False)

    # Pour une grid partiellement remplie
    jeu.grid = ['x', 'o', ' ', 'x', ' ', ' ', ' ', 'o', 'x']
    assert (jeu.winning(False) == False) and (jeu.winning(True) == False)

    # Pour une grid pleine où il y a "deux gagnants"
    jeu.grid = ['x', 'o', 'o', 'x', 'o', 'x', 'x', 'o', 'x']
    assert (jeu.winning(False) == True) and (jeu.winning(True) == True)

    # Pour une grid pleine où aucun n'est gagnant
    jeu.grid = ['o', 'x', 'o', 'x', 'o', 'x', 'x', 'o', 'x']
    assert (jeu.winning(False) == False) and (jeu.winning(True) == False)

    # Pour une grid pleine où 'x' gagne
    jeu.grid = ['o', 'x', 'o', 'x', 'o', 'o', 'x', 'x', 'x']
    assert (jeu.winning(False) == False) and (jeu.winning(True) == True)

    # Pour une grid pleine où 'o' gagne
    jeu.grid = ['x', 'o', 'x', 'o', 'x', 'x', 'o', 'o', 'o']
    assert (jeu.winning(False) == True) and (jeu.winning(True) == False)

def test_endgame():

    # Pour un jeu empty
    jeu = TicTacToe()
    assert (jeu.winning(False) == False) and (jeu.winning(True) == False)

    # Pour une grid partiellement remplie
    jeu.grid = ['x', 'o', ' ', 'x', ' ', ' ', ' ', 'o', 'x']
    assert jeu.end_game() == False

    # Pour une grid pleine où il y a "deux gagnants"
    jeu.grid = ['x', 'o', 'o', 'x', 'o', 'x', 'x', 'o', 'x']
    assert jeu.end_game() == True

    # Pour une grid pleine où aucun n'est gagnant
    jeu.grid = ['o', 'x', 'o', 'x', 'o', 'x', 'x', 'o', 'x']
    assert jeu.end_game() == False

    # Pour une grid pleine où 'x' gagne
    jeu.grid = ['o', 'x', 'o', 'x', 'o', 'o', 'x', 'x', 'x']
    assert jeu.end_game() == True

    # Pour une grid pleine où 'o' gagne
    jeu.grid = ['x', 'o', 'x', 'o', 'x', 'x', 'o', 'o', 'o']
    assert jeu.end_game() == True









