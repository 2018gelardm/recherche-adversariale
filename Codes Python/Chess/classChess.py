import chess

# Création de toutes les matrices de modification de score

pawnMod = [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
           [5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0, 5.0],
           [1.0, 1.0, 2.0, 3.0, 3.0, 2.0, 1.0, 1.0],
           [0.5, 0.5, 1.0, 2.5, 2.5, 1.0, 0.5, 0.5],
           [0.0, 0.0, 0.0, 2.0, 2.0, 0.0, 0.0, 0.0],
           [0.5, -0.5, -1.0, 0.0, 0.0, -1.0, -0.5, 0.5],
           [0.5, 1.0, 1.0, -2.0, -2.0, 1.0, 1.0, 0.5],
           [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]

knightMod = [[-5.0, -4.0, -3.0, -3.0, -3.0, -3.0, -4.0, -5.0],
             [-4.0, -2.0, 0.0, 0.0, 0.0, 0.0, -2.0, -4.0],
             [-3.0, 0.0, 1.0, 1.5, 1.5, 1.0, 0.0, -3.0],
             [-3.0, 0.5, 1.5, 2.0, 2.0, 1.5, 0.0, -3.0],
             [-3.0, 0.0, 1.5, 2.0, 2.0, 1.5, 0.0, -3.0],
             [-3.0, 0.5, 1.0, 1.5, 1.5, 1.0, 0.5, -3.0],
             [-4.0, -2.0, 0.0, 0.5, 0.5, 0.0, -2.0, -4.0],
             [-5.0, -4.0, -3.0, -3.0, -3.0, -4.0, -5.0]]

bishopMod = [[-2.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -2.0],
             [-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0],
             [-1.0, 0.0, 0.5, 1.0, 1.0, 0.5, 0.0, -1.0],
             [-1.0, 0.5, 0.5, 1.0, 1.0, 0.5, 0.5, -1.0],
             [-1.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0, -1.0],
             [-1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0],
             [-1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, -1.0],
             [-2.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -2.0]]

rookMod = [[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
           [0.5, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5],
           [-0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.5],
           [-0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.5],
           [-0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.5],
           [-0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.5],
           [-0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.5],
           [0.0, 0.0, 0.0, 0.5, 0.5, 0.0, 0.0, 0.0]]

queenMod = [[-2.0, -1.0, -1.0, -0.5, -0.5, -1.0, -1.0, -2.0],
            [-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0],
            [-1.0, 0.0, 0.5, 0.5, 0.5, 0.5, 0.0, -1.0],
            [-0.5, 0.0, 0.5, 0.5, 0.5, 0.5, 0.0, -0.5],
            [0.0, 0.0, 0.5, 0.5, 0.5, 0.5, 0.0, 0.0],
            [-1.0, 0.0, 0.5, 0.5, 0.5, 0.5, 0.0, -1.0],
            [-1.0, 0.0, 0.5, 0.0, 0.0, 0.5, 0.0, -1.0],
            [-2.0, -1.0, -1.0, -0.5, -0.5, -1.0, -1.0, -2.0]]

kingMod = [[-3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
           [-3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
           [-3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
           [-3.0, -4.0, -4.0, -5.0, -5.0, -4.0, -4.0, -3.0],
           [-2.0, -3.0, -3.0, -4.0, -4.0, -3.0, -3.0, -2.0],
           [-1.0, -2.0, -2.0, -2.0, -2.0, -2.0, -2.0, -1.0],
           [2.0, 2.0, 0.0, 0.0, 0.0, 0.0, 2.0, 2.0],
           [2.0, 3.0, 1.0, 0.0, 0.0, 1.0, 3.0, 2.0]]


class Chess:

    def __init__(self): # Création d'un plateau dans la positon de départ
        self.board = chess.Board()

    def play(self, move, player=True): # Joue le coup sur le plateau en argument (ne fais pas de test de validité du coup, besoin de le teser avant)
        if isinstance(move, str):
            self.board.push(chess.Move.from_uci(move))
        else:
            self.board.push(move)

    def cancel(self, move="foo"): # Annule le dernier coup effectué
        self.board.pop()

    def available(self): # Retourne la liste des coups possibles en notation UCI
        return self.board.legal_moves

    def end_game(self): # Fonction determinant si la partie est terminée. Considère aussi les cas de matchs nuls, mais pas d'output pour signaler un nul dans la boucle de jeu.
        return self.board.is_game_over()

    def heuristic(self): # Calcul de l'heuristique, affectation d'un score à un plateau donné
        score = 0

        whitePawns = chess.BaseBoard.pieces(self.board, 1, True)
        for i in list(whitePawns):
            line = i // 8
            column = i % 8
            score = score + 10 + pawnMod[7 - line][column]
        whiteKnights = chess.BaseBoard.pieces(self.board, 2, True)
        for i in list(whiteKnights):
            line = i // 8
            column = i % 8
            score = score + 10 + knightMod[7 - line][column]
        whiteBishops = chess.BaseBoard.pieces(self.board, 3, True)
        for i in list(whiteBishops):
            line = i // 8
            column = i % 8
            score = score + 10 + bishopMod[7 - line][column]
        whiteRooks = chess.BaseBoard.pieces(self.board, 4, True)
        for i in list(whiteRooks):
            line = i // 8
            column = i % 8
            score = score + 10 + rookMod[7 - line][column]
        whiteQueen = chess.BaseBoard.pieces(self.board, 5, True)
        for i in list(whiteQueen):
            line = i // 8
            column = i % 8
            score = score + 10 + queenMod[7 - line][column]
        whiteKing = chess.BaseBoard.pieces(self.board, 6, True)
        for i in list(whiteKing):
            line = i // 8
            column = i % 8
            score = score + 10 + kingMod[7 - line][column]

        blackPawns = chess.BaseBoard.pieces(self.board, 1, False)
        for i in list(blackPawns):
            line = i // 8
            column = i % 8
            score = score - 10 - pawnMod[line][column]
        blackKnights = chess.BaseBoard.pieces(self.board, 2, False)
        for i in list(blackKnights):
            line = i // 8
            column = i % 8
            score = score - 10 - knightMod[line][column]
        blackBishops = chess.BaseBoard.pieces(self.board, 3, False)
        for i in list(blackBishops):
            line = i // 8
            column = i % 8
            score = score - 10 - bishopMod[line][column]
        blackRooks = chess.BaseBoard.pieces(self.board, 4, False)
        for i in list(blackRooks):
            line = i // 8
            column = i % 8
            score = score - 10 - rookMod[line][column]
        blackQueen = chess.BaseBoard.pieces(self.board, 5, False)
        for i in list(blackQueen):
            line = i // 8
            column = i % 8
            score = score - 10 - queenMod[line][column]
        blackKing = chess.BaseBoard.pieces(self.board, 6, False)
        for i in list(blackKing):
            line = i // 8
            column = i % 8
            score = score - 10 - kingMod[line][column]

        return score
