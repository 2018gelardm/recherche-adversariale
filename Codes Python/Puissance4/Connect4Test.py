from pytest import *
from Puissance4.Connect4 import *

def test_class():

    game = ConnectFour()

    # Basic tests on the class
    assert (game.lines*game.rows == 42)
    assert (game.number_of_winning_alignment == 4)
    assert (len(game.grid) == 8)
    assert (len(game.grid[0]) == 9)

def test_winning():

    game = ConnectFour()

    # Test with an empty grid
    assert (game.winning(True)==0) and (game.winning(False)==0)

    # Test when the first player is winning and the second not
    game.grille= [[0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, "X", 0, 0, 0, 0, 0],
                  [0, 0, 0, "X", 0, 0, 0, 0, 0],
                  [0, 0, 0, "X", 0, 0, 0, 0, 0],
                  [0, 0, 0, "X", 0, "O", "O", "O", 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0]]
    assert (game.winning(True)==1) and (game.winning(False)==0)

    # Test when the second player is winning and the first not
    game.grille= [[0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, "X", "X", 0, 0, 0, 0, 0],
                  [0, 0, "X", "X", "O", "O", "O", "O", 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0]]
    assert (game.winning(True)==0) and (game.winning(False)==1)

def test_available():

    game = ConnectFour()

    game.grille= [[0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, "O", "X", 0, 0, 0, 0, 0],
                  [0, 0, "X", "O", 0, 0, 0, 0, 0],
                  [0, 0, "O", "O", 0, 0, 0, 0, 0],
                  [0, 0, "X", "X", 0, 0, 0, 0, 0],
                  [0, 0, "X", "X", 0, 0, 0, 0, 0],
                  [0, 0, "X", "X", "O", "O", "O", 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0]]

    assert (game.available()==[1,4,5,6,7])

def test_total_number_pawns():

    game = ConnectFour()

    game.grille= [[0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, "O", "X", 0, 0, 0, 0, 0],
                  [0, 0, "X", "O", 0, 0, 0, 0, 0],
                  [0, 0, "O", "O", 0, 0, 0, 0, 0],
                  [0, 0, "X", "X", 0, 0, 0, 0, 0],
                  [0, 0, "X", "X", 0, 0, 0, 0, 0],
                  [0, 0, "X", "X", "O", "O", "O", 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0]]

    assert(game.total_number_pawns()==15)
