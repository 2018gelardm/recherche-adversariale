import pygame
from pygame.locals import *
import copy
import time
from Puissance4.Connect4 import *
from MinimaxAlgo import *
from AlphaBetaPruning import *
import math


def main_classic():
    game = ConnectFour() # On crée le jeu (la grid,etc)
    game.display()
    end = 0
    while end == 0:
        continuing = True
        while continuing:
            row = int(input(" Où jouer (colonne entre 1 et 7) ?  ")) #On demande au joueur où veut-il jouer
            if game.is_valid(row): #on test si le coup est possible
                continuing = False
                line = game.fall_line(row)
                game.play(row, True)
                if game.number_aligned_pawns(line, row)  >= 4 : #on test si cela a permis de gagner
                    game.display()
                    print("You won !")
                    end = 1
        if end == 0: #On passe au tour de l'adversaire
            #row = minimax(jeu,4,False)[0] # On utilise l'algorithme Minmax
            row = alphaBetaPruning(game, -math.inf, math.inf, 4, False)[0]
            line = game.fall_line(row)
            game.play(row, False) #On applique le coup
            game.display() # On affiche la grid
            if game.number_aligned_pawns(line, row)  >= 4 : #on test si cela a permis de gagner
                print("You lost !")
                end = 1

def main_graphic():
    # On a besoin des positions oÃ¹ mettre les images des pions
    X_pawn = [9,105,201,297,393,489,585]
    Y_pawn = [488,392,296,200,104,8]
    # On initialise PyGame
    continuing = 1
    pygame.init()
    window = pygame.display.set_mode((900, 576), RESIZABLE)
    grid = pygame.image.load("grille.png").convert_alpha()
    window.blit(grid, (0,0))
    menu = pygame.image.load("menu.png").convert_alpha()
    window.blit(menu, (673,0))
    yellow_pawn = pygame.image.load("pion jaune.png").convert_alpha()
    red_pawn = pygame.image.load("pion rouge.png").convert_alpha()
    ai_wins = pygame.image.load("ordinateur gagne.png").convert_alpha()
    play_wins = pygame.image.load("joueur gagne.png").convert_alpha()
    pygame.display.flip()
    # On lance le jeu
    game = ConnectFour()
    # On lance la boucle de jeu
    while continuing:
        for event in pygame.event.get():
            if event.type == MOUSEBUTTONDOWN and event.button == 1: # On test si un clique est dÃ©tectÃ©
                # En fonction de la position du clic, on joue une certaine colonne
                if event.pos[0]<96 : 
                    row = 1
                    play = 1
                if event.pos[0]>100 and event.pos[0]<192 :
                    row = 2
                    play = 1
                if event.pos[0]>196 and event.pos[0]<288 :
                    row = 3
                    play = 1
                if event.pos[0]>292 and event.pos[0]<384 :
                    row = 4
                    play = 1
                if event.pos[0]>388 and event.pos[0]<480 :
                    row = 5
                    play = 1
                if event.pos[0]>484 and event.pos[0]<576 :
                    row = 6
                    play = 1
                if event.pos[0]>580 and event.pos[0]<672 :
                    row = 7
                    play = 1
                player = game.is_player() #On test si c'est bien au tour du joueur 1
                if player and game.is_valid(row) and play == 1: # On test si le coup est possible
                    line = game.fall_line(row)
                    game.play(row, player) # On applique le coup
                    window.blit(yellow_pawn, (X_pawn[row-1],Y_pawn[line-1])) # On ajoute l'image du pion au bon endroit
                    if game.number_aligned_pawns(line, row)  >= 4 : # On regarde si on a gagnÃ©
                        continuing = 0
                        window.blit(play_wins, (673,430))
                        play = 0
                    if continuing:
                        #row = minimax(jeu,4,not(joueur))[0] # On fait jouer l'ordinateur avec Minmax
                        row = alphaBetaPruning(game, -math.inf, math.inf, 4, not(player))[0]
                        line = game.fall_line(row)
                        game.play(row, not(player)) # On applique le coup dÃ©terminÃ©
                        window.blit(red_pawn, (X_pawn[row-1],Y_pawn[line-1])) # On affiche l'image
                        if game.number_aligned_pawns(line, row)  >= 4 and play == 1 : # On test si l'ordinateur gagne
                            continuing = 0
                            window.blit(ai_wins, (673,430))
                        play = 0
            if event.type == MOUSEBUTTONDOWN and event.button == 1: # On code le bouton de redÃ©marrage de la partie
                if event.pos[0]>707 and event.pos[0]<837 and event.pos[1]> 300 and event.pos[1]<410:
                    pygame.init()
                    window = pygame.display.set_mode((900, 576), RESIZABLE)
                    grid = pygame.image.load("grille.png").convert_alpha()
                    window.blit(grid, (0,0))
                    menu = pygame.image.load("menu.png").convert()
                    window.blit(menu, (673,0))
                    yellow_pawn = pygame.image.load("pion yellow.png").convert_alpha()
                    red_pawn = pygame.image.load("pion red.png").convert_alpha()
                    pygame.display.flip()
                    game = ConnectFour()
            if event.type == QUIT:
                continuing = 0
        pygame.display.flip()
    if continuing == 0: # Si la partie est finie, on attend 4 secondes et on ferme la fenÃªtre de PyGame
        time.sleep(1)



#main_classique()
main_graphic()
