from math import inf as infinity

def alphaBetaPruning(jeu,alpha,beta,depth,isMaxPlayer):

    # AI function that choice the best move
    #     :param jeu : TTT / ConnectFour / Chess
    #     :param depth: node index
    #     :param isMaxplayer: an human ( = True ) or a computer ( = False)
    #     :return: a list with [the best move, best score]


   if depth == 0 or jeu.end_game():
       score = jeu.heuristic()
       return [-1, score]

   if isMaxPlayer:
       best = [-1, -infinity]
       for cell in jeu.available():
           score = best[1]
           jeu.play(cell, isMaxPlayer)
           best[1] = max(best[1],alphaBetaPruning(jeu,alpha,beta,depth-1,not(isMaxPlayer))[1])
           jeu.cancel(cell)
           if best[1] != score:
               best[0] = cell
           if best[1] >= beta:
               return best
           alpha = max(alpha,best[1])
   else:
       best = [-1, infinity]
       for cell in jeu.available():
           score = best[1]
           jeu.play(cell, isMaxPlayer)
           best[1] = min(best[1],alphaBetaPruning(jeu,alpha,beta,depth-1,not(isMaxPlayer))[1])
           jeu.cancel(cell)
           if best[1] != score:
               best[0] = cell
           if best[1] <= alpha:
               return best
           beta = min(beta,best[1])
   return best
