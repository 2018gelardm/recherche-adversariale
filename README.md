Ce projet a été réalisé dans le cadre du projet de second semestre de première
année à CentraleSupélec.
Sur le thème de la "recherche adversariale", il s'agissait d'implémenter les
algorithmes d'IA permettant de jouer à divers jeux de plateaux, ainsi que du
développement d'une plateforme pédagogique visant l'apprentissage de ces
techniques d'IA par d'autres personnes (élèves...).

Le répertoire est composé de deux parties principales:

- La première "Code Python" contient l'ensemble des algorithmes (MiniMax /
        AlphaBeta) ainsi que les classes de définitions des différents jeux
(TicTacToe / ConnectFour / Chess)
- La seconde ("adversariale") est la plateforme Web sous la forme d'un projet Django.


Crédits:

Géraud Faye
Romain Caplier
Victor Gauthier
Benjamin Fouquet
Maxence Gélard


Sources utilisées pour ce projet :

https://www.freecodecamp.org/news/how-to-make-your-tic-tac-toe-game-unbeatable-by-using-the-minimax-algorithm-9d690bad4b37/

https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning

https://www.freecodecamp.org/news/simple-chess-ai-step-by-step-1d55a9266977/

https://www.emse.fr/~picard/files/minimax.pdf

Cours MiniMax / Élagage Alpha-Bêta de la ST2 "Théorie des jeux" (Première année à CentraleSupélec)
